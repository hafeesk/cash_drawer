# -*- coding: utf-8 -*-
# Copyright (c) 2019, Techstation and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class DrawerClosing(Document):
	def on_submit(self):
		drawer_balance = frappe.db.get_value("Drawer Balance", self.user, "name")
		if drawer_balance:
			
			doc = frappe.get_doc("Drawer Balance",self.user)
			ct = doc.append("closing_transaction",{})
                        ct.created_date = self.creation
                        ct.transaction_no = self.name
                        ct.amount = self.balance_amount
                        ct.submitted_by = frappe.session.user
			doc.date = self.to_date

			for balance in self.cash_summary:
				found = False
                                
				for current_bal in doc.balance_details:
					if current_bal.mode_of_payment == balance.mode_of_payment:
						current_bal.balance = current_bal.balance + balance.balance
						found = True
				if found == False:		
					row = doc.append("balance_details", {})
		                        row.mode_of_payment = balance.mode_of_payment
                		        row.balance = balance.balance

			doc.save()
		else:
			doc = frappe.new_doc("Drawer Balance")
			doc.date = self.to_date
			doc.user = self.user
                        ct = doc.append("closing_transaction",{})
			ct.created_date = self.creation
			ct.transaction_no = self.name
			ct.amount = self.balance_amount
                        ct.submitted_by = frappe.session.user
			for balance in self.cash_summary:
				row = doc.append("balance_details", {})
				row.mode_of_payment = balance.mode_of_payment
				row.balance = balance.balance
			doc.insert()
	
@frappe.whitelist()
def get_last_closing_date(user = ''):
	last_user_date = frappe.db.sql("""select max(to_date) from `tabDrawer Closing` where user='{}'""".format(user))
	return last_user_date

@frappe.whitelist()
def get_cash_drawer_data(from_date='',to_date='',user = ''):

    cash_drawer_data = frappe.db.sql("""select pe.name,pe.owner,pe.creation,paid_amount,payment_type,mode_of_payment,paid_from,paid_to ,reference_name,total_amount,allocated_amount,outstanding_amount from `tabPayment Entry` pe left join `tabPayment Entry Reference` per on per.parent = pe.name where date(pe.creation) between %s and %s and pe.owner=%s and pe.docstatus=1 and NOT EXISTS (select * from `tabCash Drawer Reference` cdr where cdr.document_no = pe.name)""",(from_date,to_date,user),as_dict = 1)
    
    dct_payment_mode = {}
    dct_pay_recieve = {"Pay":0,"Receive":0}
    
    if cash_drawer_data:
	for mode in cash_drawer_data:
	   if not mode['mode_of_payment'] and mode["payment_type"] == "Pay":
		pay_acc = frappe.db.sql("""select mop.name from `tabMode of Payment` mop inner join `tabMode of Payment Account` mopa on mop.name = mopa.parent where mopa.default_account = '{}'""".format(mode['paid_from']))
 		
                if pay_acc:
			mode['mode_of_payment'] = pay_acc[0][0]
		else:
			mode['mode_of_payment'] = mode['paid_from']

	   if not mode['mode_of_payment'] and mode["payment_type"] == "Receive":
                pay_acc = frappe.db.sql("""select mop.name from `tabMode of Payment` mop inner join `tabMode of Payment Account` mopa on mop.name = mopa.parent where mopa.default_account = '{}'""".format(mode['paid_to']))
                if pay_acc:
                        mode['mode_of_payment'] = pay_acc[0][0]
                else:
                        mode['mode_of_payment'] = mode['paid_to']


           if mode['allocated_amount']:
		if dct_payment_mode.has_key(mode['mode_of_payment']):
			if mode['payment_type'] == "Pay":	
				dct_payment_mode[mode['mode_of_payment']] = dct_payment_mode[mode['mode_of_payment']] - mode['allocated_amount']
			else:
				dct_payment_mode[mode['mode_of_payment']] = dct_payment_mode[mode['mode_of_payment']] + mode['allocated_amount']
		else:
			if mode['payment_type'] == "Pay":
				dct_payment_mode[mode['mode_of_payment']] = -mode['allocated_amount']
			else:
				dct_payment_mode[mode['mode_of_payment']] = mode['allocated_amount']

                if dct_pay_recieve.has_key(mode['payment_type']):
			dct_pay_recieve[mode['payment_type']] = dct_pay_recieve[mode['payment_type']] + mode['allocated_amount']
		else:
			dct_pay_recieve[mode['payment_type']] = mode['allocated_amount']
	   else:
		if dct_payment_mode.has_key(mode['mode_of_payment']):
                        if mode['payment_type'] == "Pay":       
                                dct_payment_mode[mode['mode_of_payment']] = dct_payment_mode[mode['mode_of_payment']] - mode['paid_amount']
                        else:
                                dct_payment_mode[mode['mode_of_payment']] = dct_payment_mode[mode['mode_of_payment']] + mode['paid_amount']
                else:
                        if mode['payment_type'] == "Pay":
                                dct_payment_mode[mode['mode_of_payment']] = -mode['paid_amount']
                        else:
                                dct_payment_mode[mode['mode_of_payment']] = mode['paid_amount']

                if dct_pay_recieve.has_key(mode['payment_type']):
                        dct_pay_recieve[mode['payment_type']] = dct_pay_recieve[mode['payment_type']] + mode['paid_amount']
                else:
                        dct_pay_recieve[mode['payment_type']] = mode['paid_amount']

    pos_cash_data = frappe.db.sql("""select sip.mode_of_payment,sip.amount as paid_amount,si.name, si.owner,si.creation from `tabSales Invoice` si inner join `tabSales Invoice Payment` sip on sip.parent = si.name where date(si.creation) between %s and %s and si.owner=%s and si.docstatus=1 and si.is_pos=1 and NOT EXISTS (select * from `tabCash Drawer Reference` cdr where cdr.document_no = si.name)""",(from_date,to_date,user),as_dict = 1)
    if pos_cash_data:
	for mode in pos_cash_data:
		

		mode['payment_type'] = "Receive"
		if dct_payment_mode.has_key(mode['mode_of_payment']):
                        if mode['payment_type'] == "Pay":       
                                dct_payment_mode[mode['mode_of_payment']] = dct_payment_mode[mode['mode_of_payment']] - mode['paid_amount']
                        else:
                                dct_payment_mode[mode['mode_of_payment']] = dct_payment_mode[mode['mode_of_payment']] + mode['paid_amount']
                else:
                        if mode['payment_type'] == "Pay":
                                dct_payment_mode[mode['mode_of_payment']] = -mode['paid_amount']
                        else:
                                dct_payment_mode[mode['mode_of_payment']] = mode['paid_amount']

                if dct_pay_recieve.has_key(mode['payment_type']):
                        dct_pay_recieve[mode['payment_type']] = dct_pay_recieve[mode['payment_type']] + mode['paid_amount']
                else:
                        dct_pay_recieve[mode['payment_type']] = mode['paid_amount']
    
    return cash_drawer_data,dct_payment_mode,dct_pay_recieve,pos_cash_data
