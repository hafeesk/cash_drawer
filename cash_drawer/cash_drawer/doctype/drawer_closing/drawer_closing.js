// Copyright (c) 2019, Techstation and contributors
// For license information, please see license.txt

frappe.ui.form.on('Drawer Closing', {
	refresh: function(frm) {
		if(frm.doc.__islocal){
			frm.disable_save();
		}	
		document.querySelectorAll("[data-fieldname='get_data']")[1].style.backgroundColor ="#ddd";

		frappe.call({
                                        method: 'cash_drawer.cash_drawer.doctype.drawer_balance.drawer_balance.get_drawer_settings',
                                        callback: function (r) {
						console.log(r)
                                                if(r.message.drawer_close_permission == 1){
                                                        frm.toggle_display('get_data',true)
                                                }
                                                else{

                                                        frm.toggle_display('get_data',false)
                                                }
                                        }
                });
	},
	user:function(frm){
		frappe.call({
                                method: "cash_drawer.cash_drawer.doctype.drawer_closing.drawer_closing.get_last_closing_date",
                                args: {
                                       user:frm.doc.user
                                },
                                callback: function(r) {
					console.log(r)
					frm.set_value("from_date",r.message[0][0])
				}
		});

	},
	get_data:function(frm){
		if(frm.doc.from_date && frm.doc.to_date && frm.doc.user){
		frappe.call({
				method: "cash_drawer.cash_drawer.doctype.drawer_closing.drawer_closing.get_cash_drawer_data",
				args: {
					from_date: frm.doc.from_date,
					to_date: frm.doc.to_date,
					user:frm.doc.user
				},
				callback: function(r) {
					var income_sum = 0
					var expense_sum = 0
					frm.doc.transaction = []
					frm.doc.cash_summary = []
					var dict_payment_mode = {};
					console.log(r.message)	
					try{
					for(var i=0;i<r.message[0].length;i++){
						console.log(r.message[0][i])
						var payment_table = frm.add_child("transaction")
						payment_table.mode_of_payment = r.message[0][i].mode_of_payment
						payment_table.date = r.message[0][i].creation
						if(r.message[0][i].reference_name){
							payment_table.transaction_number = r.message[0][i].reference_name
						}
						else{
							payment_table.transaction_number = r.message[0][i].name
						}
						payment_table.invoice_amount = r.message[0][i].total_amount
						if(r.message[0][i].allocated_amount){
							payment_table.paid_amount = r.message[0][i].allocated_amount 
						}else{
							payment_table.paid_amount = r.message[0][i].paid_amount						
						}
						payment_table.payment_type = r.message[0][i].payment_type
						payment_table.document_no = r.message[0][i].name
						
					}
					for(var i=0;i<r.message[3].length;i++){
						console.log(r.message[3][i])
                                                var payment_table = frm.add_child("transaction")
                                                payment_table.mode_of_payment = r.message[3][i].mode_of_payment
                                                payment_table.date = r.message[3][i].creation
                                                payment_table.transaction_number = r.message[3][i].name
                                                payment_table.invoice_amount = r.message[3][i].total_amount
                                                if(r.message[3][i].allocated_amount){
                                                        payment_table.paid_amount = r.message[3][i].allocated_amount 
                                                }else{
                                                        payment_table.paid_amount = r.message[3][i].paid_amount
                                                }
                                                payment_table.payment_type = "Receive"
						payment_table.document_no = r.message[3][i].name
                                        }

					refresh_field("transaction")
					Object.keys(r.message[1]).forEach(function(key) {

						var cash_summary = frm.add_child("cash_summary")
                                                cash_summary.mode_of_payment =  key
                                                cash_summary.balance = r.message[1][key]


					});
					refresh_field("cash_summary")
					
					frm.set_value("income_amount",r.message[2]['Receive'])
					frm.set_value("expense_amount",r.message[2]['Pay'])
					frm.set_value("balance_amount",r.message[2]['Receive']-r.message[2]['Pay'])
				}
				catch(err) {
					  console.log(err.message)
				}
				}
			});
			frm.enable_save();
			frm.set_df_property("transaction", "read_only", 1);
			frm.set_df_property("cash_summary", "read_only",  1);
		}
		else{
			frappe.msgprint("Please select From Date,To Date,User")
		}
	}
});
