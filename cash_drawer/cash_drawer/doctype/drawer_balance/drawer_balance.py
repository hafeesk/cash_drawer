# -*- coding: utf-8 -*-
# Copyright (c) 2019, Techstation and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class DrawerBalance(Document):
        def transfer_balance(self,from_account = '',to_account = '',amount = 0):
            if from_account == to_account:
                frappe.throw("From account and To account can not be same")
            
            from_found = False
            to_found = False
            for balance in self.balance_details:
                if balance.mode_of_payment == from_account:
                    from_found = True
                    if balance.balance < amount:
                        frappe.throw("Insufficient Balance")
                    balance.balance = balance.balance - amount

                if balance.mode_of_payment == to_account:
                    to_found = True
                
            if not from_found:
                frappe.throw("Insufficient Balance")

            if to_found:
                for to_balance in self.balance_details:
                    if to_balance.mode_of_payment == to_account:
                        to_balance.balance = to_balance.balance + amount
                         
            else:
                self.append('balance_details',{"mode_of_payment":to_account,"balance":amount}) 

            self.append('drawer_transfer',{"from_payment_mode":from_account,"to_payment_mode":to_account,"date":frappe.utils.today(),"amount":amount})

            self.save()



@frappe.whitelist()
def update_status(transfer_doc='',status = ''):
      
	frappe.db.sql("""update `tabTransfer Transaction` set status = '{}' where transaction_no='{}'""".format(status,transfer_doc))

@frappe.whitelist()
def get_drawer_settings():
	drawer_closing = 1
        drawer_settings = frappe.get_single('Drawer Settings')
        closing_permission = frappe.db.sql("""select count(name) from `tabDrawer Closing` where date(creation) = '{}' and owner = '{}'""".format(frappe.utils.today(),frappe.session.user))
	
	if closing_permission[0][0] > drawer_settings.number_of_closing_per_day:
		drawer_closing = 0
        return {'transfer':drawer_settings.enable_drawer_balance_transfer,'closing_per_day':drawer_settings.number_of_closing_per_day,'drawer_close_permission':drawer_closing}
