from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Cash Drawer"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Drawer Closing",
					"label":"Drawer Closing",
					"description": _("Drawer closing."),
					"onboard": 1,
				},
				{
                                        "type": "doctype",
                                        "name": "Drawer Balance",
                                        "label":"Drawer Balance",
                                        "description": _("Drawer balance."),
                                        "onboard": 1,
                                },
				{
                                        "type": "doctype",
                                        "name": "Drawer Transmutation",
                                        "label":"Drawer Transmutation",
                                        "description": _("Drawer Transmutation."),
                                        "onboard": 1,
                                },
                                    
                                

			]
		},
		{
                        "label": _("Reports"),
                        "icon": "fa fa-star",
                        "items": [
                                {
                                        "type": "report",
					"name": "Fund Summary",
					"is_query_report": True,
					"reference_doctype": "Payment Entry"
                                },
				{
                                        "type": "report",
                                        "name": "Drawer Balance",
                                        "label":"Drawer Balance",
                                        "is_query_report": True,
                                        "reference_doctype": "Drawer Balance"
                                },

                                
                        ]
                },
		{
                        "label": _("Settings"),
                        "icon": "fa fa-star",
                        "items": [
                                {
                                        "type": "doctype",
                                        "name": "Drawer Settings",
                                        "label":"Drawer Settings",
                                        "description": _("Drawer Settings."),
                                        "onboard": 1,
                                },
			]
		}

	]
